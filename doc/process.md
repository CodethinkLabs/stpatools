# Process

The process of STPA analysis is going to be an iterative ongoing process
of refinement. Following these steps should help to get off the ground and
achieve the initial draft of an STPA analysis.

## Losses

Here we consider simply what is at stake, what is considered valueable to
stakeholders ? What can go wrong ?

Listing these out is the first step in our analysis.

## Hazards

A hazard is system state or set of conditions that, together with a particular set
of worst-case environmental conditions, will lead to a loss.

Hazards are not to be confused with events that take place and inevitably cause
losses, but instead indicate a condition where losses could occur.

Hazards inform the analysis of system states that are to be avoided.

## System level constraints

Once we have identified the system level hazards we can immediately
define the system level constraints.

There are two types of system level constraints:

* Statements about hazardous conditions that must be prevented
* Statements about minimizing the amount and/or probability of loss when a hazardous condition arises

System constraints are an overview of what can go wrong in the overall system,
and as such, they will serve to inform and direct the analysis as we move on
to consider individual components and their interactions.

## Components

Break down the system into logical components. This will be the basis of
our *control structure*.

Each component has its position in a control hierarchy, and components
give instructions to other components lower in the hierarchy, while providing
feedback to components higher in the hierarchy.

### Control actions

For each component, list all of the actions it will perform to control
another component in the system.

This should be straight forward enough so long as we have a solid
definition of the overall purpose of the system and every function of
the overall system is clearly defined.

For every function the system performs, we define control actions for our
system components to describe how the function is carried out under normal circumstances.

### Feedback

For each component, list all the feedback it will provide to components higher in the hierarchy.

This should be straight forward enough so long as we have a solid
definition of the overall purpose of the system and every function of
the overall system is clearly defined.

For every function the system performs, we define feedback for our
system components to describe how the feedback is carried out under normal circumstances.


## Responsibilities

While defining our control structure (components and control actions), we
can define the component responsibilities.

The responsibilities are a refinement of system level constraints. They define
what each component needs to do so that together, the system level constraints
will be enforced.

In the context of the analysis, responsibilities will help bring attention
to the system level constraints that deserve special attention when analyzing
a given component.

### Unsafe control actions

For each *control action*, consider any *hazards* that might be caused by
this component due to the control action:

* Being provided
* Not being provided
* Being provided too early or too late
* Stopping too soon, applied too long (for continuous control actions)

Here we can draw on the *responsibilities* we've defined earlier and also
consider whether any *system constraints* for which the component in question
is responsible for, would be violated by this control action in a given
context, or system state.

### Loss scenarios

For each *unsafe control action (UCA)*, consider scenarios which might lead
to this unsafe control action occurring.

Considering loss scenarios which cause UCAs will help us consider which constraints
need to be in place in order to prevent UCAs.

Furthermore, loss scenarios also serve an interesting purpose in verification of constraints.
When we eventually use the constraints resulting from this analysis to test our system
implementation, we will also use the loss scenarios in order to develop our test procedures,
and this will give us greater confidence that our test coverage actually does verify that
our constraints are not violated.

### Controller constraints

Using the loss scenarios we've defined for our *unsafe control actions*, we
can easily derive what constraints we need to impose on our component in order
to ensure that the component does not itself introduce *unsafe control actions*
which can potentially lead to *hazards*.

## System level hazard loss scenarios

Now that we've considered *unsafe control actions* we have a pretty good idea
of what *control actions* components might perform that can lead to hazards,
and it's time to turn our focus towards the remaining component interactions.

Loss scenarios for system level hazards should be recorded for events that
could not have been handled by logical components in the control structure
we've modeled.
