
# The STPA YAML format.

This document describes the STPA YAML format in detail.

A single STPA document can be composed of a single YAML file or multiple YAML files.


## Record

Every record in the format has some common properties, inasmuch as they are uniquely identifiable dictionaries which contain some descriptive text.

```yaml
Identifier: FOO-001
Text: The quick pink pony jumps over the lazy rainbow
```

#### Fields

* `Identifier`: This is used to uniquely identify the record, and is used in the format primarily to reference other records declared in the document. See *Identifiers* for each record type for the required structure.
* `Text`: The textual description of this record, which can be a `Loss`, `Hazard`, `Component`, etc

## Losses

A loss involves something of value to stakeholders. Losses may include a loss of human life or human injury, property damage,
environmental pollution, loss of mission, loss of reputation, loss or leak of sensitive information, or any other loss that is
unacceptable to the stakeholders.

Losses are listed under the `Losses` key in the STPA files.

```yaml
Losses:
- Identifier: L-001
  Text: Rotten Sandwich
```
#### Identifiers

Losses have a simple identifier structure: L-*nnn* where *nnn* is an integer

## Hazards

A hazard is a system state or set of conditions that, together with a particular set of worst-case environmental conditions, will lead to a loss.

Hazards are listed under the `Hazards` key in the STPA files.

```yaml
Hazards:
  - Identifier: H-001
    Text: Forgotten lunchbox
    Losses: [L-001]
    Loss Scenarios:
      - Identifier: H-001-LS-01
        Text: Employee might not have time to eat their lunch, causing them to forget their lunchbox
```

#### Fields

- `Losses`: A list of previously declared `Loss` identifiers
- `Loss Scenarios`: A list of `Loss Scenario` records, which describe scenarios leading to this hazard

#### Identifiers

Hazards have a simple identifier structure: H-*nnn* where *nnn* is an integer

## Constraints

A system-level `Constraint` specifies system conditions or behaviours that need to be satisfied to prevent hazards (and ultimately prevent losses)

System-level Constraints are listed under the `Constraints` key in the STPA files.

```yaml
Constraints:
- Identifier: C-001
  Text: Employees have sufficient time to eat their lunch
  Hazards: [ H-001 ]
```

#### Fields

* `Hazards`: A list of previously declared `Hazard` identifiers

#### Identifiers

System-level Constraints have a simple identifier structure: C-*nnn* where *nnn* is an integer

## Responsibilities

`Responsibilities` define what each `Component` in the system needs to do so that *together* the
system-level `Constraints` will be enforced.

Responsibilities are listed under the `Responsibilities` key in the STPA files.

```yaml
Responsibilities:
- Identifier: R-1
  Text: Signal the end of lunch hour at the appropriate time
  Component: EX-AL
  Constraints: [ C-001 ]
```

#### Fields

* `Component`: The component identifier to which this responsibility is delegated
* `Constraints`: One or more system level `Constraint` identifiers

#### Identifiers

Responsibilities have a simple identifier structure: R-*nnn* where *nnn* is an integer

## Components

A component is one of the entities within the system which may perform some control actions on other components, or have other components
perform control actions on it.

Components are listed under the `Components` key in the STPA files.

```yaml
Components:

  # Here we define the admin as a component
- Identifier: EX-AD
  Text: Admin

  # The admin component has some control actions
  Control Actions:

  - Identifier: EX-AD-CA-001
    Text: set the alarm.
    Target: EX-AL

# Here we define the alarm as a component, which has some control actions and some feedback.
- Identifier: EX-AL
  Text: Alarm

  # The alarm component has some control actions
  Control Actions:
  - Identifier: EX-AL-CA-001
    Text: End of lunch hour alarm signal
    Target: EX-EM
            Hazards: [H-001]
            Loss Scenarios:
              - Identifier: EX-AL-LS-01
                Text: Admin may overlook daylight savings time change, and fail to set the lunch alarm correctly

    # List the contexts under which this control action could be unsafe
    Unsafe Control Actions:
    - Identifier: EX-AL-UCA-001
      Text: Send employees back to work without having finished lunch
      Reason: Timing
      Hazards: [ H-001 ]
      Loss Scenarios:
      - Identifier: EX-AL-LS-01
        Text: Admin may overlook daylight savings time change, and fail to set the lunch alarm correctly

  # The alarm components has some Feedback
  Feedback:
  - Identifier: EX-AL-FB-001
    Text: Notifies the admin that the alarm has been set off on time
    Target: EX-AD

  # Specify the constraints for this component, these constraints
  # ensure the controller does not emit unsafe control actions
  Controller Constraints:
  - Identifier: EX-AL-CC-01
    Text: Must not signal end of lunch before the end of lunch
    Unsafe Control Actions: [ EX-AL-UCA-001 ]

# The employee component is defined separately
- Identifier: EX-EM
  Text: Employees
```

#### Fields

* `Control Actions`: A list of `Control Action` declarations.
* `Feedback`: A list of `Feedback` declarations.
* `Controller Constraints`: A list of `Controller Constraints` declarations.


#### Identifiers

Components have an identifier structure: *XX*-*YY* where:

* *XX* is a capitalised mnemonic (2+ characters) for the system scope in which the component is defined
* *YY* is a capitalised mnemonic (2+ characters) for the component

## Control Actions

Control actions are the actions which one `Component` performs on another `Component`.

Control Actions are listed in the `Control Actions` field of the `Component` which issues the action.

```yaml
Control Actions:

# The control actions now also define the target component
- Identifier: EX-AL-CA-001
  Text: End of lunch hour alarm signal
  Target: EX-EM

  Unsafe Control Actions: [...]
```

#### Fields

* `Target`: The identifier of the `Component` which this `Control Action` controls.
* `Unsafe Control Actions`: A list of `Unsafe Control Action` declarations

#### Identifiers

Control Actions have an identifier structure: *XX-YY*-CA-*nnn* where:

* *XX-YY* is the identifier of the `Component` that provides this `Control Action`
* *nnn* is an integer

## Unsafe Control Actions

An `Unsafe Control Action` is a control action that, in a particular context and worst-case environment, will lead to a hazard.

Unsafe Control Actions are listed under the `Unsafe Control Action` field of their associated `Control Action`.

```yaml
Unsafe Control Actions:
  - Identifier: EX-AL-UCA-001
    Text: Send employees back to work without having finished lunch
    Reason: Timing
    Hazards: [H-001]
    Loss Scenarios:
      - Identifier: EX-AL-LS-01
        Analysis: Admin may overlook daylight savings time change, and fail to set the lunch alarm correctly
        Category: Inadequate control algorithm in Controller
        Validity: Valid
```

#### Fields

* `Reason`: The reason for which this particular `Control Action` is deemed unsafe, this can be one of four values:
  * `Providing`: Providing something which might not be safe to provide
  * `Not Providing`: Failing to provide something which it might not be safe to not provide
  * `Timing`: Too late, early or out of order
  * `Duration`: Too long or short
* `Hazards`: A list of previously declared `Hazard` identifiers

#### Identifiers

Unsafe Control Actions have an identifier structure: *XX-YY*-UCA-*nnn* where:

* *XX-YY* is the identifier of the `Component` that provides the `Control Action` associated with this UCA
* *nnn* is an integer

## Controller Constraints

A `Controller Constraint` specifies the controller behaviors that need to be satisfied to prevent `Unsafe Control Actions`.

Controller Constraints are listed under the `Controller Constraints` field of their associated `Component`.

```yaml
Controller Constraints:
- Identifier: EX-AL-CC-01
  Text: Must not signal end of lunch before the end of lunch
  Unsafe Control Actions: [ EX-AL-UCA-001 ]
```

#### Fields

* `Unsafe Control Actions`: A list of previously declared `Unsafe Control Action` identifiers

#### Identifiers

Controller Constraints have an identifier structure: *XX-YY*-CC-*nnn* where:

* *XX-YY* is the identifier of the `Component` to which the constraint applies
* *nnn* is an integer

## Loss Scenarios

A loss Scenario describes the causal factors that can lead to the unsafe control actions and hazards.

Scenarios are listed under the `Loss Scenarios` field found on either `Hazard` or `Unsafe Control Action` records.

```yaml
Loss Scenarios:
  - Identifier: EX-AL-LS-01
    Text: Admin may overlook daylight savings time change, and fail to set the lunch alarm correctly
    Category: Inadequate control algorithm in Controller
    Validity: Valid
```

#### Fields

`Category` is an optional text field, intended for use with a checklist of Loss Scenario types

`Validity` is an optional enumerated field, indicating whether an identified Scenario is valid in the context of the analysis. If it is included, the permitted values are:

* "Unknown": Scenario has not yet been evaluated for validity
* "Valid": Scenario is valid for this system boundary and control structure
* "Out Of Scope": Scenario is valid, but out of scope for the defined system boundary
* "Invalid": Scenario is not valid for this system boundary and control structure

#### Identifiers

Scenarios have an identifier structure: *XX-YY*-LS-*nnn* where:

* *XX-YY* is the identifier of the `Component` or the `Hazard` to which the Scenario applies
* *nnn* is an integer

## Feedback

Feedback is information that one `Component` provides to another `Component`, which forms part of a feedback control loop.

Feedback is listed in the `Feedback` field of the `Component` that provides the feedback.

```yaml
Feedback:

# The control actions now also define the target component
- Identifier: EX-AL-FB-001
  Text: Notifies the admin that the alarm has been set off on time.
  Target: EX-AD
```

#### Fields

* `Target`: The identifier of the `Component` that receives the provided `Feedback`.

#### Identifiers

Feedback has an identifier structure: *XX-YY*-FB-*nnn* where:

* *XX-YY* is the identifier of the `Component` that provides this `Feedback`
* *nnn* is an integer
