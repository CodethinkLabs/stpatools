# Losses

## L-1

Loss of life or injury to people

## L-2

Loss of or damage to vehicle

## L-3

Loss of or damage to objects outside of vehicle

## L-4

Loss of mission

## L-5

Loss of customer satisfaction

## L-6

Loss of sensitive information

## L-7

Environmental loss


# Hazards

## H-1

Aircraft violate minimum separation standards in flight


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-4](#l-4), [L-5](#l-5)

## H-2

Aircraft airframe integrity is lost


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-4](#l-4), [L-5](#l-5)

## H-3

Aircraft leaves designated taxiway, runway, or apron on ground


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4

Aircraft comes too close to other objects on the ground


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.1

Deceleration is insufficient upon landing, rejected takeoff, or during taxiing


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.2

Asymmetric deceleration maneuvers aircraft toward other objects


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.3

Deceleration occurs after V1 point during takeoff


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.4

Excessive acceleration provided while taxiing


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.5

Asymmetric acceleration maneuvers aircraft toward other objects


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.6

Acceleration is insufficient during takeoff


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.7

Acceleration is provided during landing or when parked


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.8

Acceleration continues to be applied during rejected takeoff


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.9

Insufficient steering to turn along taxiway, runway, or apron path


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-4.10

Steering maneuvers aircraft off the taxiway, runway, or apron path


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)

## H-5

Aircraft comes too close to other objects on the ground


Possible Losses: [L-1](#l-1), [L-2](#l-2), [L-5](#l-5)


# System Constraints

| Constraint | Description | Hazards |
| ---- | ---- | ---- |
| SC-6.1 | Deceleration must occur within TBD seconds of landing or rejected takeoff at a rate of at least TBD m/s2  | [H-4.1](#h-41) |
| SC-6.2 | Asymmetric deceleration must not lead to loss of directional control or cause aircraft to depart taxiway, runway, or apron  | [H-4.2](#h-42) |
| SC-6.3 | Deceleration must not be provided after V1 point during takeoff  | [H-4.3](#h-43) |

# Component responsibilities

| ID | Component | Constraints | Description |
| ---- | ---- | ---- | ---- |
| R-1 | [Brake](#brake) | [SC-6.1](#sc-61) | Decelerate wheels when commanded by BSCU or Flight Crew  |
| R-2 | [BSCU](#bscu) | [SC-6.1](#sc-61) | Actuate brakes when requested by flight crew  |
| R-3 | [BSCU](#bscu) | [SC-6.2](#sc-62) | Pulse brakes in case of a skid (Anti-skid)  |
| R-4 | [BSCU](#bscu) | [SC-6.1](#sc-61) | Automatically engage brakes on landing or rejected takeoff (Autobrake)  |
| R-5 | [Crew](#crew) | [SC-6.1](#sc-61), [SC-6.3](#sc-63) | Decide when braking is needed  |
| R-6 | [Crew](#crew) | [SC-6.1](#sc-61) | Decide how braking will be done: Autobrake, normal braking, or manual brakiing  |
| R-7 | [Crew](#crew) | [SC-6.1](#sc-61) | Configure BSCU and Autobrake to prepare for braking  |
| R-8 | [Crew](#crew) | [SC-6.1](#sc-61), [SC-6.2](#sc-62) | Monitor braking and disable BSCU, manually brake in case of malfunction  |

# BSCU

Brake System Control Unit

## Unsafe Control Action Summary

| Control Action | Providing | Not Providing | Timing | Duration |
| ---- | ---- | ---- | ---- | ---- |
| [BSCU-CA-1](#bscu-ca-1) | [BSCU-UCA-002](#bscu-uca-002), [BSCU-UCA-005](#bscu-uca-005), [BSCU-UCA-006](#bscu-uca-006) | [BSCU-UCA-001](#bscu-uca-001) | [BSCU-UCA-003](#bscu-uca-003) | [BSCU-UCA-004](#bscu-uca-004) |

## Control Actions

### BSCU-CA-1

Brake

**Target:** [Brake](#brake)

#### Unsafe Control Actions

##### BSCU-UCA-001

Autobrake does not provide the Brake control action during landing roll when the BSCU is armed 

**Reason:** Not Providing

**Possible Hazards:** [H-4.1](#h-41)

**Loss Scenarios:**
* **BSCU-LS-001**
  - **Category**: Failures involving the controller
  - **Validity**: Valid
  - **Analysis**: The BSCU Autobrake physical controller fails during landing roll when BSCU is armed, causing the Brake control action to not be provided. As a result, insufficient deceleration may be provided upon landing. 

##### BSCU-UCA-002

Autobrake provides Brake control action during a normal takeoff 

**Reason:** Providing

**Possible Hazards:** [H-4.3](#h-43), [H-4.6](#h-46)

**Loss Scenarios:**
* **BSCU-LS-002.1**
  - **Analysis**: The BSCU is armed and the aircraft begins landing roll. The BSCU does not provide the Brake control action because the BSCU incorrectly believes the aircraft has already come to a stop. This flawed process model will occur if the received feedback momentarily indicates zero speed during landing roll. The received feedback may momentarily indicate zero speed during anti-skid operation, even though the aircraft is not stopped. 
* **BSCU-LS-002.2**
  - **Analysis**: The BSCU is armed and the aircraft begins landing roll. The BSCU does not provide the Brake control action because the BSCU incorrectly believes the aircraft is in the air and has not touched down. This flawed process model will occur if the touchdown indication is not received upon touchdown. The touchdown indication may not be received when needed if any of the following occur: Wheels hydroplane due to a wet runway (insufficient wheel speed), Wheel speed feedback is delayed due to filtering used, Conflicting air/ground indications due to crosswind landing, Failure of wheel speed sensors, Failure of air/ground switches. 

##### BSCU-UCA-003

Autobrake provides the Brake control action too late (>TBD seconds) after touchdown 

**Reason:** Timing

**Possible Hazards:** [H-4.1](#h-41)

**Loss Scenarios:**
* **BSCU-LS-003**
  - **Category**: Inadequate control algorithm
  - **Validity**: Valid
  - **Analysis**: The aircraft lands, but processing delays within the BSCU result in the Brake control action being provided too late. As a result, insufficient deceleration may be provided upon landing. 

##### BSCU-UCA-004

Autobrake stops providing the Brake control action too early (before TBD taxi speed attained) when aircraft lands 

**Reason:** Duration

**Possible Hazards:** [H-4.1](#h-41)

##### BSCU-UCA-005

Autobrake provides Brake control action with an insufficient level of braking during landing roll 

**Reason:** Providing

**Possible Hazards:** [H-4.1](#h-41)

##### BSCU-UCA-006

Autobrake provides Brake control action with directional or asymmetrical braking during landing roll 

**Reason:** Providing

**Possible Hazards:** [H-4.1](#h-41), [H-4.2](#h-42)

## Constraints

| Constraint | Description | UCAs |
| ---- | ---- | ---- |
| BSCU-CC-001 | Autobrake must provide the Brake control action during landing roll when the BSCU is armed  | [BSCU-UCA-001](#bscu-uca-001) |
| BSCU-CC-002 | Autobrake must not provide Brake control action during a normal takeoff  | [BSCU-UCA-002](#bscu-uca-002) |
| BSCU-CC-003 | Autobrake must provide the Brake control action within TBD seconds after touchdown  | [BSCU-UCA-003](#bscu-uca-003) |
| BSCU-CC-004 | Autobrake must not stop providing the Brake control action before TBD taxi speed is attained during landing roll  | [BSCU-UCA-004](#bscu-uca-004) |
| BSCU-CC-005 | Autobrake must not provide less than TBD level of braking during landing roll  | [BSCU-UCA-005](#bscu-uca-005) |
| BSCU-CC-006 | Autobrake must not provide directional or asymmetrical braking during landing roll  | [BSCU-UCA-006](#bscu-uca-006) |

# Brake

Physical Wheel Brakes


## Feedback

### Brake-FB-1

Physical brake applied

**Target:** [BSCU](#bscu)

# Crew

The flight crew

## Unsafe Control Action Summary

| Control Action | Providing | Not Providing | Timing | Duration |
| ---- | ---- | ---- | ---- | ---- |
| [Crew-CA-01](#crew-ca-01) | [Crew-UCA-002](#crew-uca-002) | [Crew-UCA-001](#crew-uca-001) | [Crew-UCA-003](#crew-uca-003) |  |

## Control Actions

### Crew-CA-01

Power Off BSCU

**Target:** [BSCU](#bscu)

#### Unsafe Control Actions

##### Crew-UCA-001

Crew does not provide BSCU Power Off when abnormal WBS behavior occurs 

**Reason:** Not Providing

**Possible Hazards:** [H-4.1](#h-41), [H-4.4](#h-44), [H-4.7](#h-47)

**Loss Scenarios:**
* **Crew-LS-001**
  - **Analysis**: Abnormal WBS behavior occurs and a BSCU fault indication is provided to the crew. The crew does not power off the BSCU [Crew-UCA-1] because the operating procedures did not specify that the crew must power off the BSCU upon receiving a BSCU fault indication. 

##### Crew-UCA-002

Crew provides BSCU Power Off when Anti-Skid functionality is needed and WBS is functioning normally 

**Reason:** Providing

**Possible Hazards:** [H-4.1](#h-41)

##### Crew-UCA-003

Crew powers off BSCU too early before Autobrake or Anti-Skid behavior is completed when it is needed 

**Reason:** Timing

**Possible Hazards:** [H-4.1](#h-41)

# Wheels

Physical Wheels


## Feedback

### Wheels-FB-1

Wheel speed

**Target:** [BSCU](#bscu)

