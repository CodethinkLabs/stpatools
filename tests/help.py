from click.testing import CliRunner

from stpatools._validate import validate
from stpatools._render import render


def test_validate_help():
    runner = CliRunner()
    result = runner.invoke(validate, ["--help"])
    assert result.exit_code == 0
    assert "--help" in result.output
    assert "--version" in result.output
    assert result.exit_code == 0


def test_render_help():
    runner = CliRunner()
    result = runner.invoke(render, ["--help"])
    assert result.exit_code == 0
    assert "--help" in result.output
    assert "--version" in result.output
    assert result.exit_code == 0
