#!/usr/bin/env python3

import sys
from typing import List
import click

from . import (
    load_stpa,
    STPAError,
    Loss,
    Hazard,
    Component,
    SystemConstraint,
    Responsibility,
)
from ._print_version import print_version
from . import _render_markdown, _render_asciidoc


@click.command(help="Render the STPA as markdown (default) or Asciidoc")
@click.option("--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True)
@click.option(
    "--output",
    "-o",
    type=click.Path(dir_okay=False, writable=True),
    required=True,
    help="Output file to store the rendered markup",
)
@click.option(
    "--syntax",
    "-s",
    type=click.Choice(["markdown", "asciidoc"]),
    help="Choose syntax to render document",
)
@click.argument("files", nargs=-1, type=click.Path())
def render(files, output, syntax):
    try:
        model = load_stpa(files)
    except STPAError as e:
        click.echo(f"Error: {e}", err=True)
        sys.exit(1)
    renderer = _render_markdown
    if syntax == "markdown":
        renderer = _render_markdown
    elif syntax == "asciidoc":
        renderer = _render_asciidoc

    losses: List[Loss] = []
    hazards: List[Hazard] = []
    components: List[Component] = []
    constraints: List[SystemConstraint] = []
    responsibilities: List[Responsibility] = []
    components: List[Component] = []

    # Collect and sort all of the toplevel records
    for _, record in model.records.items():
        if isinstance(record, Loss):
            losses.append(record)
        elif isinstance(record, Hazard):
            hazards.append(record)
        elif isinstance(record, SystemConstraint):
            constraints.append(record)
        elif isinstance(record, Component):
            components.append(record)
        elif isinstance(record, Responsibility):
            responsibilities.append(record)

    report_lines: List[str] = []

    # Format markdown for various records into the report_lines list
    renderer.format_losses(report_lines, losses)
    renderer.format_hazards(report_lines, hazards)
    renderer.format_system_constraints(report_lines, constraints)
    renderer.format_responsibilities(report_lines, responsibilities)
    renderer.format_components(report_lines, components)

    # Join it all into a single string and write the report
    report = "\n".join(report_lines)
    with open(output, "w", encoding="utf-8") as f:
        f.write(report)


def anchor_identifier(identifier: str) -> str:
    """Generate an anchor for an identifier (Asciidoc only)"""
    anchor = identifier.replace(".", "_")
    anchor = anchor.replace(" ", "-")
    return anchor
