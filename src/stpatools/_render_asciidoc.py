# !/usr/bin/env python3

import sys
from typing import List
import re
import click

from .record import (
    Record,
    Loss,
    Hazard,
    Component,
    ControlAction,
    Feedback,
    ControllerConstraint,
    SystemConstraint,
    Responsibility,
    Scenario,
    UnsafeControlAction,
    UnsafeControlActionType,
    LossScenarioValidityType,
)


def anchor_identifier(identifier: str) -> str:
    """Generate an anchor for an identifier (Asciidoc only)"""
    anchor = identifier.replace(".", "_")
    anchor = anchor.replace(" ", "-")
    return anchor


def link_identifier(identifier: str) -> str:
    """Generate a link to an identifier"""
    link = anchor_identifier(identifier)
    link_id = f"<<{link},{identifier}>>"
    return link_id


def sortByIdentifier(item: Record):
    """gets the identifier for a record to be used with .sort()"""
    identifier = item.identifier  # e.g 'CA-10' 'CA-1'
    # can't sort by identifier directly as 'CA-1' 'CA-2'... 'CA-10' would sort as 'CA-1' 'CA-10' 'CA-2'
    # So need human sorting
    # adapted from https://nedbatchelder.com/blog/200712/human_sorting.html

    def atoi(text):
        """test if value is int and return the value as an int or return text"""
        return int(text) if text.isdigit() else text

    # Turn a string into a list of string and number chunks. e.g "z23a" -> ["z", 23, "a"]
    return [atoi(c) for c in re.split(r"(\d+)", identifier)]


def format_scenarios(report_lines: List[str], scenarios: List[Scenario]) -> None:
    """Formats the scenarios into the report lines"""
    if scenarios:
        report_lines.append("*Loss Scenarios:*")
        report_lines.append("")
        scenarios.sort(key=sortByIdentifier)
        for scenario in scenarios:
            text = scenario.text.replace("\n", " ")
            report_lines.append(f"* *{scenario.identifier}*")
            if scenario.category:
                report_lines.append(f"** *Category*: {scenario.category}")
            if not scenario.validity == LossScenarioValidityType.UNKNOWN:
                report_lines.append(f"** *Validity*: {scenario.validity.value}")
            report_lines.append(f"** *Analysis*: {text}")
            report_lines.append("")


def format_losses(report_lines: List[str], losses: List[Loss]) -> None:
    """Formats the losses into the report lines"""
    report_lines.append("== Losses")
    report_lines.append("")
    losses.sort(key=sortByIdentifier)
    for loss in losses:
        anchor = anchor_identifier(loss.identifier)
        report_lines.append(f"[#{anchor}]")
        report_lines.append(f"=== {loss.identifier}")
        report_lines.append("")
        report_lines.append(f"{loss.text}")
        report_lines.append("")
    report_lines.append("")


def format_hazards(report_lines: List[str], hazards: List[Hazard]) -> None:
    """Formats the hazards into the report lines"""
    report_lines.append("== Hazards")
    report_lines.append("")
    hazards.sort(key=sortByIdentifier)
    for hazard in hazards:
        anchor = anchor_identifier(hazard.identifier)
        report_lines.append(f"[#{anchor}]")
        report_lines.append(f"=== {hazard.identifier}")
        report_lines.append("")
        report_lines.append(f"{hazard.text}")
        report_lines.append("")
        if hazard.losses:
            hazard_losses = ", ".join(link_identifier(loss.identifier) for loss in hazard.losses)
            report_lines.append(f"Possible Losses: {hazard_losses}")
            report_lines.append("")
        format_scenarios(report_lines, hazard.scenarios)
    report_lines.append("")


def format_system_constraints(report_lines: List[str], constraints: List[SystemConstraint]) -> None:
    """Formats the system constraints into the report lines"""
    report_lines.append("== System Constraints")
    report_lines.append("")
    report_lines.append('[cols="^2,<5,^2",options="header",]')
    report_lines.append("|===")
    report_lines.append("|Constraint |Description |Hazards")
    constraints.sort(key=sortByIdentifier)
    for constraint in constraints:
        anchor = anchor_identifier(constraint.identifier)
        identifier = f"[[{anchor}]]{constraint.identifier}"
        if constraint.hazards:
            hazard_list = ", ".join(link_identifier(hazard.identifier) for hazard in constraint.hazards)
        description = constraint.text.replace("\n", " ")
        report_lines.append(f"| {identifier} | {description} | {hazard_list} ")
    report_lines.append("|===")
    report_lines.append("")


def format_responsibilities(report_lines: List[str], responsibilities: List[Responsibility]) -> None:
    """Formats the responsibilities into the report lines"""
    report_lines.append("== Component responsibilities")
    report_lines.append("")
    report_lines.append('[cols="^1,^2,^2,<5",options="header",]')
    report_lines.append("|===")
    report_lines.append("|ID |Component |Constraints |Description")
    responsibilities.sort(key=sortByIdentifier)
    for responsibility in responsibilities:
        identifier = responsibility.identifier.replace("\n", " ")
        component = link_identifier(responsibility.component.identifier)
        constraints = ", ".join(link_identifier(constraint.identifier) for constraint in responsibility.constraints)
        description = responsibility.text.replace("\n", " ")
        report_lines.append(f"| {identifier} | {component} | {constraints} | {description} ")
    report_lines.append("|===")
    report_lines.append("")


def format_control_actions(report_lines: List[str], control_actions: List[ControlAction]) -> None:
    """Format a component's control actions into the report lines"""
    if control_actions:
        report_lines.append("=== Control Actions")
        report_lines.append("")
        control_actions.sort(key=sortByIdentifier)
        for action in control_actions:
            text = action.text.replace("\n", " ")
            anchor = anchor_identifier(action.identifier)
            report_lines.append(f"[#{anchor}]")
            report_lines.append(f"==== {action.identifier}")
            report_lines.append("")
            report_lines.append(f"{text}")
            report_lines.append("")
            report_lines.append(f"*Target:* {link_identifier(action.target.identifier)}")
            report_lines.append("")

            if action.unsafe_control_actions:
                report_lines.append("#### Unsafe Control Actions")
                report_lines.append("")
                action.unsafe_control_actions.sort(key=sortByIdentifier)
                for unsafe in action.unsafe_control_actions:
                    text = unsafe.text.replace("\n", " ")
                    report_lines.append(f"##### {unsafe.identifier}")
                    report_lines.append("")
                    report_lines.append(f"{text}")
                    report_lines.append("")
                    report_lines.append(f"*Reason:* {unsafe.reason.value}")
                    report_lines.append("")
                    unsafe_hazards = ", ".join(link_identifier(hazard.identifier) for hazard in unsafe.hazards)
                    report_lines.append(f"*Possible Hazards:* {unsafe_hazards}")
                    report_lines.append("")
                    format_scenarios(report_lines, unsafe.scenarios)


def format_feedback(report_lines: List[str], feedback_list: List[Feedback]) -> None:
    """Format a component's feedback into the report lines"""
    if feedback_list:
        report_lines.append("=== Feedback")
        report_lines.append("")
        feedback_list.sort(key=sortByIdentifier)
        for feedback in feedback_list:
            text = feedback.text.replace("\n", " ")
            anchor = anchor_identifier(feedback.identifier)
            report_lines.append(f"[#{anchor}]")
            report_lines.append(f"==== {feedback.identifier}")
            report_lines.append("")
            report_lines.append(f"{text}")
            report_lines.append("")
            report_lines.append(f"*Target:* {link_identifier(feedback.target.identifier)}")
            report_lines.append("")


def format_controller_constraints(report_lines: List[str], constraints: List[ControllerConstraint]) -> None:
    """Format a component's constraints into the report lines"""
    if constraints:
        report_lines.append("=== Constraints")
        report_lines.append("")
        report_lines.append('[cols="^2,<5,^2",options="header",]')
        report_lines.append("|===")
        report_lines.append("|Constraint |Description |UCAs")
        constraints.sort(key=sortByIdentifier)
        for constraint in constraints:
            identifier = constraint.identifier.replace("\n", " ")
            description = constraint.text.replace("\n", " ")
            ucas = ", ".join(link_identifier(uca.identifier) for uca in constraint.unsafe_control_actions)
            report_lines.append(f"| {identifier} | {description} | {ucas} ")
        report_lines.append("|===")
        report_lines.append("")


def format_uca_summary(report_lines: List[str], component: Component) -> None:
    """Format a the summary table of all unsafe control actions report lines"""

    # Omit the table if the component has no control actions
    if component.control_actions:

        report_lines.append("=== Unsafe Control Action Summary")
        report_lines.append("")

        report_lines.append('[cols="^,^,^,^,^",options="header",]')
        report_lines.append("|===")
        report_lines.append("|Control Action |Providing |Not Providing |Timing |Duration")

        # Sort control actions
        component.control_actions.sort(key=sortByIdentifier)

        # Process each action in turn
        for action in component.control_actions:

            action_id = link_identifier(action.identifier)

            # Sort and collect the various UCA types
            providing_ucas_list: List[UnsafeControlAction] = []
            not_providing_ucas_list: List[UnsafeControlAction] = []
            timing_ucas_list: List[UnsafeControlAction] = []
            duration_ucas_list: List[UnsafeControlAction] = []
            for unsafe in action.unsafe_control_actions:
                if unsafe.reason == UnsafeControlActionType.PROVIDING:
                    providing_ucas_list.append(unsafe)
                elif unsafe.reason == UnsafeControlActionType.NOT_PROVIDING:
                    not_providing_ucas_list.append(unsafe)
                elif unsafe.reason == UnsafeControlActionType.TIMING:
                    timing_ucas_list.append(unsafe)
                elif unsafe.reason == UnsafeControlActionType.DURATION:
                    duration_ucas_list.append(unsafe)
                else:
                    click.echo(
                        f"{unsafe.provenance}: Unsupported unsafe control action reason '{unsafe.reason.value}'",
                        err=True,
                    )
                    sys.exit(1)

            # Format a row for this control action in the table
            providing_ucas = ", ".join(link_identifier(uca.identifier) for uca in providing_ucas_list)
            not_providing_ucas = ", ".join(link_identifier(uca.identifier) for uca in not_providing_ucas_list)
            timing_ucas = ", ".join(link_identifier(uca.identifier) for uca in timing_ucas_list)
            duration_ucas = ", ".join(link_identifier(uca.identifier) for uca in duration_ucas_list)
            report_lines.append(
                f"| {action_id} | {providing_ucas} | {not_providing_ucas} | {timing_ucas} | {duration_ucas} "
            )
        report_lines.append("|===")

    report_lines.append("")


def format_components(report_lines: List[str], components: List[Component]) -> None:
    """Formats the components into the report lines"""
    components.sort(key=sortByIdentifier)
    for component in components:
        anchor = anchor_identifier(component.identifier)
        report_lines.append(f"[#{anchor}]")
        report_lines.append(f"== {component.identifier}")
        report_lines.append("")
        report_lines.append(f"{component.text}")
        report_lines.append("")

        # Format the UCA summary table
        format_uca_summary(report_lines, component)

        # Format the component's control actions
        format_control_actions(report_lines, component.control_actions)
        format_feedback(report_lines, component.feedback)
        format_controller_constraints(report_lines, component.controller_constraints)
    report_lines.append("")
