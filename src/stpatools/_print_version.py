import click
from . import __version__


def print_version(ctx, _, value):
    if not value or ctx.resilient_parsing:
        return

    click.echo(__version__)
    ctx.exit()
