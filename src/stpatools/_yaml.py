import os
from typing import overload, List, Any, TextIO, Tuple, Generator, Optional

# This can be imported directly from `typing` if we require Python >= 3.8
from typing_extensions import Literal
from ruamel import yaml
from ruamel.yaml.comments import CommentedSeq, CommentedMap
from ruamel.yaml.scalarstring import ScalarString

from .exceptions import LoadError


class Provenance:
    """Defines the provenance in the YAML where a record or attribute was declared"""

    def __init__(self, filename: str, line: int, column: int) -> None:
        self.filename: str = os.path.abspath(filename)
        """The YAML filename"""

        self.line: int = line + 1
        """The line number"""

        self.column: int = column
        """The column number"""

    def __str__(self):
        return f"{self.filename} line {self.line} column {self.column}"


def load_yaml_data(data: TextIO) -> CommentedMap:
    """Load some YAML data"""

    try:
        contents = yaml.load(data, yaml.loader.RoundTripLoader, preserve_quotes=True)
    except (yaml.scanner.ScannerError, yaml.composer.ComposerError, yaml.parser.ParserError) as e:
        raise LoadError(f"Malformed YAML:\n\n{e.problem}\n\n{e.problem_mark}\n") from e

    if not isinstance(contents, dict):
        # Special case allowance for None, when the loaded file has only comments in it.
        if contents is None:
            contents = {}
        else:
            raise LoadError(
                f"YAML file has content of type '{type(contents).__name__}' instead of expected type 'dict'"
            )

    return contents


def load_yaml_file(filename: str) -> CommentedMap:
    """Load a YAML file, expects the toplevel to be a dictionary"""
    try:
        with open(filename, encoding="utf-8") as f:
            try:
                return load_yaml_data(f)
            except LoadError as e:
                raise LoadError(f"Error loading {filename}: {e}") from e

    except FileNotFoundError as e:
        raise LoadError(f"Could not find file at {filename}") from e
    except IsADirectoryError as e:
        raise LoadError(f"{filename} is a directory, expected a yaml file.") from e


def validate_mapping(filename, mapping: CommentedMap, valid_keys: List[str]) -> None:
    """Validate that only the expected keys are in a mapping"""

    invalid_key = next((key for key in mapping if key not in valid_keys), None)
    if invalid_key:
        line, column = mapping.lc.value(invalid_key)
        provenance = Provenance(filename, line, column)
        raise LoadError(f"{provenance}: Invalid key: {invalid_key}")


def load_key(provenance: Provenance, node: dict, key: str, *, required: bool = True) -> Any:
    """Load a value from a YAML dictionary, raising an error if the key doesn't exist"""
    try:
        value = node[key]
    except KeyError as e:
        if required:
            raise LoadError(f"{provenance}: Could not find expected key: {key}") from e
        # If it was not a required key, we'll just return None
        value = None

    return value


# Some type decorations, this function is guaranteed to return an `str` object if `required` is unspecified, or explicitly True.
#
@overload
def load_str_key(provenance: Provenance, node: dict, key: str) -> str:
    ...


@overload
def load_str_key(provenance: Provenance, node: dict, key: str, *, required: Literal[True]) -> str:
    ...


@overload
def load_str_key(provenance: Provenance, node: dict, key: str, *, required: Literal[False]) -> Optional[str]:
    ...


# This pylint issue should be eventually fixed with newer versions of pylint,
# see upstream report: https://github.com/PyCQA/pylint/issues/2561
#
def load_str_key(provenance, node, key, *, required=True):  # pylint: disable=too-many-return-statements
    """Like load_key(), but guarantees a str type will be returned"""
    value = load_key(provenance, node, key, required=required)

    # Can only happen if `required` was specified False
    if value is None:
        return value

    if not isinstance(value, (ScalarString, str)):
        raise LoadError(f"{provenance}: Incorrect type for key '{key}', expected a string")

    return str(value)


def load_sequence(filename: str, mapping: CommentedMap, key: str) -> CommentedSeq:
    """An accessor which is guaranteed to return a sequence, or raise an error"""
    value = mapping.get(key, CommentedSeq([]))

    if not isinstance(value, CommentedSeq):
        line, col = mapping.lc.value(key)
        provenance = Provenance(filename, line, col)
        raise LoadError(f"{provenance}: Incorrect type for key '{key}', expected a list")

    return value


def load_str_list(filename: str, mapping: CommentedMap, key: str) -> CommentedSeq:
    """An accessor which is guaranteed to return a sequence of strings"""
    sequence: CommentedSeq = load_sequence(filename, mapping, key)

    for item in sequence:
        if not isinstance(item, (ScalarString, str)):
            index = sequence.index(item)
            line, column = sequence.lc.item(index)
            provenance = Provenance(filename, line, column)
            raise LoadError(f"{provenance}: List item is not a string")

    return sequence


def sequence_items(
    filename: str, mapping: CommentedMap, key: str
) -> Generator[Tuple[Provenance, CommentedMap], None, None]:
    """A generator which iterates over a sequence, yielding a provenance and mapping for each element"""
    sequence: CommentedSeq = load_sequence(filename, mapping, key)
    for item in sequence:
        if not isinstance(item, CommentedMap):
            index = sequence.index(item)
            line, column = sequence.lc.item(index)
            provenance = Provenance(filename, line, column)
            raise LoadError(f"{provenance}: List item is not a dict")

        index = sequence.index(item)
        line, column = sequence.lc.item(index)
        provenance = Provenance(filename, line, column)

        yield provenance, item
