#!/usr/bin/env python3

import sys
import textwrap
import re
import pydot  # type: ignore
import click

from . import load_stpa, STPAError
from ._print_version import print_version
from .record import IncompleteReport


@click.command(help="Validate that the STPA document can be loaded, and observe completeness")
@click.option("--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True)
@click.option(
    "--diagram",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True),
    help="Validate that a diagram can be loaded, and observe completeness",
)
@click.argument("files", nargs=-1, type=click.Path())
def validate(files, diagram):

    try:
        model = load_stpa(files)
    except STPAError as e:
        click.echo(f"Error: {e}", err=True)
        sys.exit(1)
    list_incomplete = model.list_incomplete()

    if diagram:
        try:
            graph = pydot.graph_from_dot_file(diagram)[0]
        except TypeError:
            click.echo(
                "Error: You have used the --diagram option but have not provided a valid diagram file", err=True
            )
            sys.exit(1)

        validate_diagram(graph, model, list_incomplete)

    incomplete = [str(i) for i in list_incomplete]
    incomplete_report = "\n".join(incomplete)
    incomplete_report = textwrap.indent(incomplete_report, "    ")

    if incomplete:
        click.echo(f"WARNING, the data model is incomplete in the following ways:\n\n{incomplete_report}", err=True)


def validate_diagram(graph, model, list_incomplete):
    nodes = {str(node) for node in graph.get_node_list()}
    if bool(graph.get_subgraphs):
        subgraphs = {str(subgraph) for subgraph in graph.get_subgraph_list()}
        nodes.update(subgraphs)
    dot_id_set = set()
    for dot_id in nodes:
        id_re = re.findall(r'URL="#([\w-]+)"', dot_id)
        dot_id_set.update(id_re)
    yml_id_set = set()
    append_record = None
    for record in model.records:
        if "Component" in str(model.records.get(record)):
            yml_id_set.add(record)
            append_record = record
    id_set_str = ", ".join(dot_id_set.symmetric_difference(yml_id_set))
    if id_set_str:
        list_incomplete.append(
            IncompleteReport(
                model.records.get(append_record),
                f"Missing identifier found inside the diagram: {id_set_str}",
            )
        )
